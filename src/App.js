import React, { useEffect, useState } from "react";

import logo from './logo.svg';
import './App.css';
import api from 'axios';

const weatherUrl = process.env.WEATHER_URL;
const feedUrl = process.env.FEED_URL;

const Weather = () => {
  const [weatherData, setweatherData] = useState({});

  useEffect(() => {
    getWeatherData();
  }, []);

  const getWeatherData = () => {
    api.get(`${weatherUrl}/city`)
    .then(({data}) => setweatherData(data))
    .catch((error) => console.log(error))
  }

  const temp = weatherData && weatherData.main && weatherData.main.temp;

  return (
    <div>
     {temp && <div>Hello! It is {temp}°C in {weatherData.name}</div>}
    </div>
  )
}

const Feed = () => {
  const [feedData, setfeedData] = useState({});

  useEffect(() => {
    getFeedData();
  }, []);

  const getFeedData = () => {
    api.get(`${feedUrl}/feed`)
    .then(({data}) => setfeedData(data))
    .catch((error) => console.log(error))
  }

  return (
    <div>
      {feedData.title && <h1>News from {feedData.title}</h1>}
      {feedData.items && <ul>
        {feedData.items.map(item => <li><a href={item.url}>{item.title}</a></li>)}
      </ul>}
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Welcome to Newther</h1>
        <h4>a programming news feed along with a tinge of weather news</h4>
        <img src={logo} className="App-logo" alt="logo" />
        <Weather />
        <Feed />
      </header>
    </div>
  );
}

export default App;
